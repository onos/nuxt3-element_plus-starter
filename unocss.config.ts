import {
    defineConfig,
    presetAttributify,
    presetIcons,
    presetMini,
    presetUno,
    transformerDirectives,
    transformerVariantGroup,
  } from 'unocss'
  
  export default defineConfig({
  
    presets: [
      presetUno(),
      presetMini(),
      presetAttributify(),
      presetIcons({
        scale: 1.2,
      }),
    ],
    transformers: [
      transformerDirectives(),
      transformerVariantGroup(),
    ]
  })
  